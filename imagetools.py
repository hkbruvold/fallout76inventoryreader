import cv2
import numpy as np


def open_image(img_path):
    return cv2.imread(img_path)


def save_image(image, img_path):
    cv2.imwrite(img_path, image)


def crop_image(image, x1, x2, y1, y2):
    return image[y1:y2, x1:x2]


def convert_to_grayscale(image):
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


def apply_threshold(image, thresh=127):
    _, thresh = cv2.threshold(image, thresh, 255, cv2.THRESH_BINARY)
    return thresh


def invert_image(image):
    return cv2.bitwise_not(image)


def conditional_invert_image(image):
    avg_row = np.average(image, axis=0)
    avg = np.average(avg_row, axis=0)
    if avg < 127:
        return invert_image(image)
    return image


def is_mostly_yellow(image):
    avg_row = np.average(image, axis=0)
    avg = np.average(avg_row, axis=0)
    if len(avg) != 3:
        return avg

    # Score should give an indication of how far from pure yellow the color is
    score = avg[0] + 256 - avg[1] + 256 - avg[2]
    if score < 300:
        return True
    return False


def convert_pil_to_cv2(pil_image):
    return cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGB2BGR)
