from OCR import get_string_from_image, get_string_line_from_image
import imagetools as imtools
from config import Config
from inventorydatabase import InventoryDatabase
from itemdatabase import ItemDatabase
#from whitelisthandler import WhitelistHandler
from GUI import InventoryReaderGUI
import workerthreads

import cv2
import threading


class InventoryReader:
    def __init__(self, configfile="config.json"):
        self.config_handler = Config(configfile)
        self.config = self.config_handler.config

        self.perk_types = {"ballisticammo": "Ballistic Ammo",
                           "chems": "Chems",
                           "energyammo": "Energy Ammo",
                           "explosives": "Explosives",
                           "foodndrink": "Food and Drink",
                           "junk": "Junk"}

        self.inventory_handler = InventoryDatabase()
        self.itemdb_handler = ItemDatabase()
        #self.whitelist_handler = WhitelistHandler()

        self.inventory_lock = threading.Lock() # Lock for adding items to inventory
        self.screenshot_timer_event = threading.Event()

        self.GUI = InventoryReaderGUI(self.perk_types)

        # Store variable used for Item database configurator
        self.idbc_last_selected = ""
        self.idbc_listbox_list = []

        # Store variable used for Item database deleter
        self.idbd_listbox_list = []

        # Configure screenshot button
        self.GUI.screenshot_button.config(command=self.toggle_screenshot_timer)

        # Configure refresh button
        self.GUI.refresh_button.config(command=self.refresh_entire_treeview)

        # Configure screenshot delay related items
        self.GUI.delay_spinbox.delete(0, "end")
        self.GUI.delay_spinbox.insert(0, self.config["screenshot_delay"])
        self.GUI.learning_mode_checkbox.config(command=self.update_screenshot_delay_from_config)
        self.GUI.delay_spinbox.config(command=self.update_screenshot_timer_delay)

        # Start Screenshot taker daemon
        self.screenshot_timer = workerthreads.ScreenshotTimer(args=(self.screenshot_timer_event,
                                                                    self.config["screenshot_delay"],),
                                    kwargs={"config": self.config,
                                    "add_to_inventory_function": self.update_inventory_safe,
                                    "get_learning_mode_function": self.GUI.learning_mode.get,
                                    "learn_new_item_function": self.learn_new_item,
                                    "get_storage_mode_function": self.GUI.invstash_radiobutton_var.get,
                                    "refresh_entire_treeview_function": self.refresh_entire_treeview})
        self.screenshot_timer.start()

        # Configure Clear Inventory/Stash button
        self.GUI.clear_inventory_button.config(command=lambda: self.clear_inventory("inventory"))
        self.GUI.clear_stash_button.config(command=lambda: self.clear_inventory("stash"))

        # Populate treeview with inventory
        self.refresh_entire_treeview()

        # Configure inventory buttons
        for category in self.GUI.categories:
            self.GUI.inventory_buttons[category].config(command=lambda ct=category: self.add_inventory_to_treeview(storage="inventory", category=ct))
            self.GUI.stash_buttons[category].config(command=lambda ct=category: self.add_inventory_to_treeview(storage="stash", category=ct))

        # Configure Perk Rank Spinboxes
        for perk_type in self.perk_types.keys():
            self.GUI.perk_card_spinboxes[perk_type].config(command=lambda ptype=perk_type: self.update_perk_spinboxes(ptype))
        self.update_perk_spinboxes_from_config()

        # Configure Item database config button
        self.GUI.itemdb_config_button.config(command=self.create_itemdb_config_window)

        # Configure Item database deleter button
        self.GUI.itemdb_delete_button.config(command=self.create_itemdb_delete_window)

    def update_inventory_safe(self, item_name, amount, storage_mode):
        self.inventory_lock.acquire()
        try:
            if item_name in self.itemdb_handler.database:
                self.inventory_handler.set(item_name, amount, storage_mode)
                self.GUI.add_log_line("Found " + item_name.ljust(45, ".") + ", Amount: " + str(amount), color="green")
            else:
                self.GUI.add_log_line(item_name + " was not found in database", color="red")
        finally:
            self.inventory_lock.release()

    def update_screenshot_timer_delay(self):
        try:
            new_value = float(self.GUI.delay_spinbox.get())
            self.screenshot_timer.delay = new_value
            if self.GUI.learning_mode.get():
                self.config["learner_delay"] = new_value
            else:
                self.config["screenshot_delay"] = new_value
            self.config_handler.save_config()
        except ValueError:
            print("Invalid screenshot delay value")

    def update_screenshot_delay_from_config(self):
        self.GUI.delay_spinbox.delete(0, "end")
        if self.GUI.learning_mode.get():
            self.GUI.delay_spinbox.insert(0, self.config["learner_delay"])
        else:
            self.GUI.delay_spinbox.insert(0, self.config["screenshot_delay"])
        self.update_screenshot_timer_delay()

    def update_perk_spinboxes(self, perk_type):
        try:
            new_value = int(self.GUI.perk_card_spinboxes[perk_type].get())
            self.config["perk_rank_%s" % perk_type] = new_value
            self.config_handler.save_config()
        except ValueError:
            print("Invalid %s value" % perk_type)
        except KeyError:
            print("Couldn't find perk_card_%s in config" % perk_type)
        self.refresh_entire_treeview()

    def update_perk_spinboxes_from_config(self):
        for perk_type in self.perk_types.keys():
            spinbox = self.GUI.perk_card_spinboxes[perk_type]
            if "perk_rank_%s"%perk_type not in self.config:
                self.config["perk_rank_%s"%perk_type] = 0
                self.config_handler.save_config()
            spinbox.delete(0, "end")
            spinbox.insert(0, self.config["perk_rank_%s"%perk_type])

    def toggle_screenshot_timer(self):
        if self.screenshot_timer_event.is_set():
            self.screenshot_timer_event.clear()
            self.GUI.screenshot_button.config(text="Start Screenshot Timer")
        else:
            self.screenshot_timer_event.set()
            self.GUI.screenshot_button.config(text="Stop Screenshot Timer")
        self.refresh_entire_treeview()

    def take_single_screenshot(self, e=None):
        workerthreads.SingleScreenshot(kwargs={"config": self.config,
                                               "add_to_inventory_function": self.update_inventory_safe}).start()

    def get_item_weight(self, item, use_perks=False):
        itemdb = self.itemdb_handler.database
        if item not in itemdb:
            print("WARNING: Couldn't find %s in item database"%item)
            return

        weight_mult = 1.0
        if use_perks:
            perk_type = itemdb[item]["perk"]

            perk_rank = 0
            if "perk_rank_%s"%perk_type in self.config:
                perk_rank = self.config["perk_rank_%s"%perk_type]

                if perk_rank != 0:
                    if perk_type in self.config["perk_card_effects"]:
                        weight_mult = 1 - self.config["perk_card_effects"][perk_type][perk_rank-1]
                    else:
                        weight_mult = 1 - self.config["perk_card_effects"]["default"][perk_rank - 1]

        return itemdb[item]["weight"] * weight_mult

    def get_weight_sorted_inventory(self, storage="inventory", category="ALL"):
        inv = self.inventory_handler.inventory[storage]
        itemdb = self.itemdb_handler.database
        sorted_inventory = []

        for item, amount in inv.items():
            if item not in itemdb:
                continue

            # Handle category
            if category != "ALL" and category != itemdb[item]["category"]:
                continue

            weight = self.get_item_weight(item, storage == "inventory")
            totweight = amount * weight
            sorted_inventory.append({"name": item, "weight": weight, "totweight": totweight, "amount": amount})

        def get_tot_weight(item):
            return item["totweight"]

        return sorted(sorted_inventory, key=get_tot_weight, reverse=True)

    def get_category_sorted_items(self):
        itemdb = self.itemdb_handler.database
        sorted_items = []
        categories = {
            "UNDEFINED": [],
            "WEAPONS": [],
            "APPAREL": [],
            "AID": [],
            "MISC": [],
            "HOLO": [],
            "NOTES": [],
            "JUNK": [],
            "MODS": [],
            "AMMO": []
        }

        for item in itemdb.keys():
            categories[itemdb[item]["category"]].append(item)

        for cat, cat_list in categories.items():
            sorted_items += sorted(cat_list)

        return sorted_items

    def clear_inventory(self, storage="both"):
        self.inventory_handler.reset_inventory(storage)
        self.refresh_entire_treeview()

    def add_inventory_to_treeview(self, storage="inventory", category="ALL"):
        sorted_list = self.get_weight_sorted_inventory(storage, category)

        self.GUI.clear_inventory_list(storage)
        self.GUI.add_sorted_item_list(sorted_list, storage)

        # Update total weight to button
        tot_weight = 0
        for item in sorted_list:
            tot_weight += item["totweight"]
        self.GUI.set_button_weight(tot_weight, storage, category)

    def refresh_entire_treeview(self):
        for category in reversed(self.GUI.categories):
            self.add_inventory_to_treeview(storage="inventory", category=category)
            self.add_inventory_to_treeview(storage="stash", category=category)

    def learn_new_item(self, item_name, weight, category):
        self.GUI.add_log_line("Learned: %s, weight: %f, category: %s" %(item_name, weight, category))
        #self.whitelist_handler.add(item_name)
        self.itemdb_handler.set(item_name, weight, category)

    def change_perk_type(self, item_name, perk_type):
        if item_name not in self.itemdb_handler.database:
            return

        self.itemdb_handler.set(item_name, perk_type=perk_type)

    def save_itemdb_config_listbox(self):
        if not self.idbc_last_selected:
            return

        itemdb = self.itemdb_handler.database

        # Make changes to itemdb
        selected_list = self.GUI.idbc_listbox.curselection()
        for index, item in enumerate(self.idbc_listbox_list):
            if index in selected_list:
                self.itemdb_handler.set(item, perk_type=self.idbc_last_selected)
            else:
                if self.itemdb_handler.get(item)["perk"] == self.idbc_last_selected:
                    self.itemdb_handler.set(item, perk_type="None")

    def change_itemdb_config_listbox(self, to_perk):
        self.save_itemdb_config_listbox()
        self.GUI.clear_idbc_listbox()
        self.update_itemdb_config_listbox(to_perk)
        self.idbc_last_selected = to_perk

    def update_itemdb_config_listbox(self, perk_type):
        if perk_type not in self.perk_types:
            print("update_itemdb_config_window() got unknown perk type")
            return

        sorted_items = self.get_category_sorted_items()
        self.idbc_listbox_list = sorted_items

        # Add items to listbox and activate items of selected perk type
        for index, item in enumerate(sorted_items):
            self.GUI.add_item_to_bottom_idbc_listbox(item)
            if self.itemdb_handler.database[item]["perk"] == perk_type:
                self.GUI.idbc_listbox.selection_set(index)

    def update_itemdb_delete_listbox(self):
        sorted_items = self.get_category_sorted_items()
        self.idbd_listbox_list = sorted_items

        for item in sorted_items:
            self.GUI.add_item_to_bottom_idbd_listbox(item)

    def delete_itemdb_delete_selection(self):
        selected_list = self.GUI.idbd_listbox.curselection()

        for index, item in enumerate(self.idbd_listbox_list):
            if index in selected_list:
                self.itemdb_handler.remove(item, save=False)

        self.itemdb_handler.remove("", save=True)
        self.GUI.clear_idbd_listbox()
        self.update_itemdb_delete_listbox()

    def create_itemdb_config_window(self):
        self.GUI.create_itemdb_config_window()

        first_perk = list(self.perk_types.keys())[0]
        self.idbc_last_selected = first_perk
        self.update_itemdb_config_listbox(first_perk)
        self.GUI.idbc_buttons[first_perk].select()

        for perk_type in self.perk_types.keys():
            self.GUI.idbc_buttons[perk_type].config(command=lambda pt=perk_type: self.change_itemdb_config_listbox(pt))

        self.GUI.idbc_save_button.config(command=self.save_itemdb_config_listbox)

    def create_itemdb_delete_window(self):
        self.GUI.create_itemdb_delete_window()
        self.update_itemdb_delete_listbox()
        self.GUI.idbd_delete_button.config(command=self.delete_itemdb_delete_selection)


if __name__ == "__main__":
    inventory_reader = InventoryReader()

    inventory_reader.add_inventory_to_treeview()
    inventory_reader.GUI.mainloop()
