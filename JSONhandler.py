import json
import os


def load(filename):
    with open(filename, 'r') as f:
        try:
            return json.load(f)
        except ValueError: # ValueError when trying to parse an empty file
            return {}


def save(object, filename):
    if os.path.dirname(filename) and not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))

    with open(filename, 'w') as f:
        json.dump(object, f, sort_keys=True, indent=4)
