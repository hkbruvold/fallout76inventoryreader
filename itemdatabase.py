import os

import JSONhandler as jsonh


class ItemDatabase:
    def __init__(self, datadir="itemdatabase", original_filename="manual_items.json", learned_filename="learned_items.json"):
        self.datadir = datadir
        self.original_filename = original_filename
        self.learned_filename = learned_filename

        try:
            self.original_data = self.load_json(original_filename)
        except FileNotFoundError:
            self.original_data = {}
            self.save_json(self.original_data, original_filename)
        try:
            self.learned_data = self.load_json(learned_filename)
        except FileNotFoundError:
            self.learned_data = {}
            self.save_json(self.learned_data, learned_filename)
        self.database = self.combine_dicts(self.learned_data, self.original_data)

    def load_json(self, filename):
        fullpath = os.path.join(self.datadir, filename)
        return jsonh.load(fullpath)

    def save_json(self, object, filename):
        fullpath = os.path.join(self.datadir, filename)
        jsonh.save(object, fullpath)

    def combine_dicts(self, dict1, dict2):
        return {**dict1, **dict2}

    def get(self, item):
        if item in self.database:
            return self.database[item]
        return None

    def set(self, item, weight=None, category="UNDEFINED", perk_type="idc", save=True):
        # If no weight specified, use database weight
        if weight == None and item in self.database:
            weight = self.database[item]["weight"]

        # Avoid changing category to UNDEFINED if we already know the category
        if category == "UNDEFINED" and item in self.database:
            category = self.database[item]["category"]

        # Avoid changing perk type to None if we already have a perk for the item
        if perk_type == "idc":
            if item in self.database:
                perk_type = self.database[item]["perk"]
            else:
                perk_type = "None"
        
        item_data = {"weight": weight,
                     "category": category,
                     "perk": perk_type}
        self.learned_data[item] = item_data
        if item not in self.original_data:
            self.database[item] = item_data

        if save:
            self.save_json(self.learned_data, self.learned_filename)

    def remove(self, item, save=True):
        if item in self.learned_data:
            del self.learned_data[item]

        if item not in self.original_data:
            self.database = self.combine_dicts(self.learned_data, self.original_data)

        if save:
            self.save_json(self.learned_data, self.learned_filename)
