import os

import JSONhandler as jsonh


class InventoryDatabase:
    def __init__(self, datadir="inventory", inventory_filename="inventory.json"):
        self.datadir = datadir
        self.inventory_filename = inventory_filename

        try:
            self.inventory = self.load_json(inventory_filename)
        except FileNotFoundError:
            self.inventory = {"inventory": {}, "stash": {}}
            self.save_json(self.inventory, self.inventory_filename)

    def load_json(self, filename):
        fullpath = os.path.join(self.datadir, filename)
        return jsonh.load(fullpath)

    def save_json(self, object, filename):
        fullpath = os.path.join(self.datadir, filename)
        jsonh.save(object, fullpath)

    def get(self, item, storage="inventory"):
        if item in self.inventory[storage]:
            return self.inventory[storage][item]
        return None

    def set(self, item, amount, storage="inventory"):
        if type(item) == str and type(amount) == int:
            self.inventory[storage][item] = amount
            self.save_json(self.inventory, self.inventory_filename)

    def reset_inventory(self,storage):
        if storage == "both":
            self.inventory = {"inventory": {}, "stash": {}}
        elif storage == "inventory":
            self.inventory["inventory"] = {}
        elif storage == "stash":
            self.inventory["stash"] = {}
        self.save_json(self.inventory, self.inventory_filename)
