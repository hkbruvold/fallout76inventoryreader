import os

import JSONhandler as jsonh


class WhitelistHandler:
    def __init__(self, whitelist="whitelist.json", datadir=""):
        self.whitelist_filename = whitelist
        self.datadir = datadir

        try:
            self.whitelist = jsonh.load(whitelist)
        except FileNotFoundError:
            self.whitelist = []
            jsonh.save(self.whitelist, whitelist)

    def load_json(self, filename):
        fullpath = os.path.join(self.datadir, filename)
        return jsonh.load(fullpath)

    def save_json(self, object, filename):
        fullpath = os.path.join(self.datadir, filename)
        jsonh.save(object, fullpath)

    def is_whitelisted(self, item):
        return item in self.whitelist

    def add(self, item):
        if item not in self.whitelist and type(item) == str:
            self.whitelist.append(item)
            self.save_json(self.whitelist, self.whitelist_filename)

    def remove(self, item):
        if item in self.whitelist:
            self.whitelist.remove(item)
            self.save_json(self.whitelist, self.whitelist_filename)
