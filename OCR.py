import pytesseract


def get_string_from_image(image):
    return pytesseract.image_to_string(image, lang="eng")


def get_string_line_from_image(image):
    return pytesseract.image_to_string(image, lang="eng", config="--psm 7")