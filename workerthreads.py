import threading
import time
import os
import re # For filtering text

from screenshot import fullscreen_screenshot
import imagetools as imtools
from OCR import get_string_line_from_image


class ProcessImage(threading.Thread):
    """Process a screenshot image"""
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, *, daemon=False):
        super().__init__(group=group, target=target, name=name,
                         daemon=daemon)
        self.args = args
        self.kwargs = kwargs

        if len(args) < 1:
            print("ProcessImage didn't receive image as args")

        if kwargs == None or "config" not in kwargs:
            print("ProcessImage didn't receive config data")

    def run(self):
        image = self.args[0]
        full_config = self.kwargs["config"]
        learning_mode = self.kwargs["get_learning_mode_function"]()
        storage_mode = self.kwargs["get_storage_mode_function"]()
        config = None

        # Determine what coordinates to use based on screenshot size
        if str(len(image)) in full_config["screenshot_positions"]:
            config = full_config["screenshot_positions"][str(len(image))]
        else:
            print("Couldn't find positions for image size of %i" %len(image))

        # Create sub-images for each inventory item and create Thread to handle them
        cropped_list = []
        fip = config[storage_mode]["list_position"]
        step = config[storage_mode]["list_step_size"]
        inventory_list_size = config[storage_mode]["list_size"]
        c_pos = config[storage_mode]["category_position"]
        w_pos = config["weight_position"]
        for n in range(inventory_list_size):
            cropped_list.append(imtools.crop_image(image, fip[0], fip[1], fip[2] + n * step, fip[3] + n * step))

        if learning_mode:
            # Crop the weight area
            weight_crop = imtools.crop_image(image, w_pos[0], w_pos[1], w_pos[2], w_pos[3])

            # Crop the category area
            category_crop = imtools.crop_image(image, c_pos[0], c_pos[1], c_pos[2], c_pos[3])
            for cropped in cropped_list:
                if imtools.is_mostly_yellow(cropped):
                    ProcessHighlightedLine(args=(cropped,weight_crop,category_crop,), kwargs=self.kwargs).start()
        else:
            for cropped in cropped_list:
                ProcessInventoryLine(args=(cropped,storage_mode,), kwargs=self.kwargs).start()


class ProcessInventoryLine(threading.Thread):
    """Process an image of a single line expected to be from inventory"""
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, *, daemon=False):
        super().__init__(group=group, target=target, name=name,
                         daemon=daemon)
        self.args = args
        self.kwargs = kwargs

        if len(args) < 2:
            print("ProcessInventoryLine didn't receive image, and/or storage mode")

    def run(self):
        image = self.args[0]
        storage_mode = self.args[1]
        add_to_inventory_function = self.kwargs["add_to_inventory_function"]

        # Do some pre-processing on image to help the OCR process
        image = imtools.convert_to_grayscale(image)
        #image = imtools.apply_threshold(image)
        image = imtools.conditional_invert_image(image)

        # Use Tesseract OCR to read image
        item_text = get_string_line_from_image(image)

        # Process the text
        splitted = item_text.split(" (")
        if len(splitted) == 2:
            item_name = splitted[0]
            try:
                amount = int(splitted[1].split(")")[0])
            except ValueError:
                print("ERROR: ProcessInventoryLine trying to convert %s into item and amount resulted in ValueError"%item_text)
                print("Saving the image to OCRfailures\\%s.png for debugging purposes"%item_text)
                imtools.save_image(image, os.path.join("OCRfailures", "%s.png"%item_text))
                return
        elif len(splitted) == 1:
            item_name = splitted[0]
            amount = 1

        # Filter text to eliminate unwanted symbols
        item_name = re.sub(r'([^\s\w]|_)+', '', item_name)
        item_name = item_name.strip() # Because removing symbol ahead of name creates whitespace

        add_to_inventory_function(item_name, amount, storage_mode)


class ProcessHighlightedLine(threading.Thread):
    """Process an image of a single highlighted line expected to be from inventory"""
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, *, daemon=False):
        super().__init__(group=group, target=target, name=name,
                         daemon=daemon)
        self.args = args
        self.kwargs = kwargs

        if len(args) < 3:
            print("ProcessHighlightedLine didn't receive image, weight crop and category crop")

    def run(self):
        image = self.args[0]
        weight_crop = self.args[1]
        category_crop = self.args[2]
        config = self.kwargs["config"]
        learn_new_item_function = self.kwargs["learn_new_item_function"]
        text_list = ["", "", ""]

        # Do some pre-processing on image to help the OCR process
        image = imtools.convert_to_grayscale(image)
        # image = imtools.apply_threshold(image)
        image = imtools.conditional_invert_image(image)
        weight_crop = imtools.convert_to_grayscale(weight_crop)
        weight_crop = imtools.conditional_invert_image(weight_crop)
        category_crop = imtools.convert_to_grayscale(category_crop)
        category_crop = imtools.conditional_invert_image(category_crop)

        # Use Tesseract OCR to read image using threads
        def apply_ocr(image, result_list, index):
            text = get_string_line_from_image(image)
            result_list[index] = text

        image_t = threading.Thread(target=apply_ocr, args=(image,text_list,0,))
        weight_t = threading.Thread(target=apply_ocr, args=(weight_crop,text_list,1,))
        category_t = threading.Thread(target=apply_ocr, args=(category_crop,text_list,2,))
        image_t.start()
        weight_t.start()
        category_t.start()
        image_t.join()
        weight_t.join()
        category_t.join()

        item_text = text_list[0]
        weight_text = text_list[1]
        category_text = text_list[2]

        # Process the text
        splitted = item_text.split("(")
        if len(splitted) == 2:
            item_name = splitted[0].strip()
            try:
                amount = int(splitted[1].split(")")[0])
            except ValueError:
                print(
                    "ERROR: ProcessHighlightedLine trying to convert %s into item and amount resulted in ValueError" % item_text)
                print("Saving the image to OCRfailures\\%s.png for debugging purposes" % item_text)
                imtools.save_image(image, os.path.join("OCRfailures", "%s.png" % item_text))
                return
        elif len(splitted) == 1:
            item_name = splitted[0].strip()
            amount = 1

        # Process the weight
        try:
            weight = float(weight_text)
        except ValueError:
            print(
                "ERROR: ProcessHighlightedLine trying to convert %s into float(weight) resulted in ValueError" %
                weight_text)
            print("Saving the image to OCRfailures\\%s.png for debugging purposes" % weight_text)
            imtools.save_image(weight_crop, os.path.join("OCRfailures", "%s.png" % weight_text))
            return

        # Process the category
        category = "UNDEFINED"
        if category_text == "MY STASH BOX (STASH)":
            pass
        else:
            if "MY " in category_text:
                category_text = category_text[3:]

            if category_text in config["categories"]:
                category = category_text

        # Filter text to eliminate unwanted symbols
        item_name = re.sub(r'([^\s\w]|_)+', '', item_name)
        item_name = item_name.strip() # Because removing symbol ahead of name creates whitespace

        learn_new_item_function(item_name, weight, category)


class ScreenshotTimer(threading.Thread):
    """Take screenshots on a timer, screenshots will automatically be passed to ProcessImage"""
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, *, daemon=True):
        super().__init__(group=group, target=target, name=name,
                         daemon=daemon)
        self.args = args
        self.kwargs = kwargs

        if len(args) < 2:
            return
        self.delay = args[1]

    def run(self):
        event = self.args[0]
        while True:
            # Wait for event to start
            event.wait()

            pil_img = fullscreen_screenshot()
            cv2_img = imtools.convert_pil_to_cv2(pil_img)

            ProcessImage(args=(cv2_img,), kwargs=self.kwargs).start()

            time.sleep(self.delay)

class SingleScreenshot(threading.Thread):
    """Take a single screenshot and process the image"""
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, *, daemon=True):
        super().__init__(group=group, target=target, name=name,
                         daemon=daemon)
        self.args = args
        self.kwargs = kwargs

        if kwargs == None or "config" not in kwargs or "add_to_inventory_function" not in kwargs:
            print("SingleScreenshot is missing some arguments in kwargs")

    def run(self):
        pil_img = fullscreen_screenshot()
        cv2_img = imtools.convert_pil_to_cv2(pil_img)

        ProcessImage(args=(cv2_img,), kwargs=self.kwargs).start()
