import JSONhandler as jsonh


class Config:
    def __init__(self, configfile="config.json"):
        self.config_filename = configfile
        self.config = self.load_config()

    def load_config(self):
        try:
            return jsonh.load(self.config_filename)
        except FileNotFoundError:
            print("Error loading config file")
            return {}

    def save_config(self):
        jsonh.save(self.config, self.config_filename)
